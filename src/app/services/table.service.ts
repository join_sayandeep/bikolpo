import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Petition } from '../models/petition';
import { environment } from '../../environments/environment';

// decorators
@Injectable({
    providedIn: 'root'
})

export class TableService {
    readonly ROOT_URL = environment.BASE_URL + '/petitions';
    constructor(private http: HttpClient) {}

    getRecords(page: number, pageSize: number): Observable<Petition[]> {
       return this.http.get<Petition[]>(this.ROOT_URL+"?page="+page+"&size="+pageSize);
    }
}

