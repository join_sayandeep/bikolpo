import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Petition } from '../../models/petition';
import { TableService } from '../../services/table.service';
import { SharedService } from './shared.service';
@Component({
  templateUrl: 'tables.component.html'
})
export class TablesComponent implements OnInit {
  petitions: Petition[] = [];
  ifLoading: boolean = false;

  constructor(private readonly tabledata: TableService, private EditData: SharedService, private readonly router: Router) { }
  page: number = 0;
  loadTableData(page: number, pageSize: number) {
    this.ifLoading = true;
    this.petitions = [];
     this.tabledata.getRecords(page, pageSize).subscribe(
       data => {
         this.petitions = data;
         this.ifLoading = false;
       }
     );
   }

   ngOnInit() {
       this.loadTableData(this.page, 10);
   }

   getPrev() {
    
    if(this.page!=0) {
      this.page--;
      this.loadTableData(this.page, 10);
    }
     

   }

   getNext() {
    
    if(this.petitions.length ==10){
      this.page++;
      this.loadTableData(this.page, 10);
    }
    
  }
  onEdit(pety: Petition){
    
    this.EditData.setPetition(pety);
    this.router.navigate(['base/petition']);
  }
  
}
