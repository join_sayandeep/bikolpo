import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Petition } from '../../models/petition';
import { SharedService } from './shared.service';

@Component({
  templateUrl: 'petedit.component.html'
})
export class PetitionEditComponent implements OnInit {

  constructor(private sharedData: SharedService) { }
  editData : Petition;
  loading = false;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';
  ngOnInit(){
    this.editData = this.sharedData.getPetition();
    console.log(this.editData);
  }
  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

     
  
    onUpdate(data: any) {
      this.loading = false;
     console.warn(data.value);

      // const data: Login = {
      //   username: 'tester1',
      //   password: 'tester1'
      // };

        // const headersdata = new HttpHeaders({
        //   'Access-Control-Allow-Origin': '*'
        // });

      // this.NewLoginData = this.http.post(this.ROOT_URL, data.value, {headers: headersdata}).subscribe((valdata) => {
      //   this.api_data = valdata['token'];
      //   localStorage.setItem('user_key', this.api_data);
      //   this.router.navigate(['/base/tables']);
      // }, error => {
      //   this.err = 'Wrong Username & Password';
      //   this.loading = false;
      // });

    }

}
