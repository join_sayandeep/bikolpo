import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Petition } from '../../models/petition';



@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public editData : any = [];
  private sub = new BehaviorSubject(this.editData);
  subj = this.sub.asObservable();

  send(value: any) {
    this.sub.next(value);
  }
  
  obpetition: Petition;

  setPetition(ob: Petition){
    this.obpetition = ob;
  }

  getPetition(){
    return this.obpetition;
  }
}