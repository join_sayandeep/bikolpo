import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Login } from './login';
import { Router, ActivatedRoute } from '@angular/router';

// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent  {
    readonly ROOT_URL = 'http://localhost:8080/login';

    constructor(private http: HttpClient, private router: Router) { }
    
    loading = false;
    api_data: any;
    res_status: any;
    NewLoginData: any;
    token: any;
    uname: string;
    err: string = '';
    onSubmit(data: any) {
      this.loading = true;
     console.warn(data.value);

      // const data: Login = {
      //   username: 'tester1',
      //   password: 'tester1'
      // };

        const headersdata = new HttpHeaders({
          'Access-Control-Allow-Origin': '*'
        });

      this.NewLoginData = this.http.post(this.ROOT_URL, data.value, {headers: headersdata}).subscribe((valdata) => {
        this.api_data = valdata['token'];
        localStorage.setItem('user_key', this.api_data);
        this.router.navigate(['/base/tables']);
      }, error => {
        this.err = 'Wrong Username & Password';
        this.loading = false;
      });

    }

 }
