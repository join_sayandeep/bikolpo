export interface Petition {
      "name": string;
      "district": string;
      "gender": string;
      "age": number;
      "maritalStatus": string;
      "residenceType": string;
      "phone": string;
      'dependents': string;
      'healthCondition': string;
      'qualification': string;
      'occupation': string;
      'professionalQualification': string;
      'licenseHolder': string;
      'permitHolder': string;
      'jobCardHolder': string;
      "addresses" : PetitionAddress[];
      "socialSecurities": string[];
      'officialIds': string[];
      'requirements': string[];
}
export interface PetitionAddress {
          "line1": string;
          "line2": string;
          "ps": string;
          "pinCode": string;
          "addressType": string;
}